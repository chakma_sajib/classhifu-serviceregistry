## Microservice:
“An architecture pattern that allows you functionally decompose the software into a manageable and independent deployable unit”

#### For synchronous requests – Proxy such as Nginx, Amazon API Gateway, etc.
#### For Asynchronous requests – Queues such as RabbitMQ, Amazon SQS etc.

Here, We are using **Spring boot**, **Spring Cloud**, **Eureka**, **Cloud Config Server**, **Zipkin** and **Sleuth** to learn  **Microservice architecture**

Currently, We have two services: -  User Service {git repo link: [User Service](https://gitlab.com/chakma_sajib/classhifu-userservice) }, and - Department Service[Department Service](https://gitlab.com/chakma_sajib/classhifu-departmentservice)


[![Screen-Shot-2021-03-14-at-10-59-09-PM.png](https://i.postimg.cc/pLGpKGZ8/Screen-Shot-2021-03-14-at-10-59-09-PM.png)](https://postimg.cc/9D9mhBsF)
